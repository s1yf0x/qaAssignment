package com.herokuapp.waakoopserver.api;

import com.herokuapp.waakoopserver.utilities.RestUtil;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchApi {

    private static Logger logger = LoggerFactory.getLogger(SearchApi.class);

    RestUtil restUtil = new RestUtil();

    @Step
    public Response searchProduct(String product) {
        logger.info("Searching for {}", product);
        return restUtil.search(product);
    }

}
