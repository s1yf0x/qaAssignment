package com.herokuapp.waakoopserver.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Properties;

public class Configuration {

    private final static Logger logger = LoggerFactory.getLogger(Configuration.class);


    Properties properties = new Properties();

    public  Configuration() {
        try (InputStream inputStream = Configuration.class.getResourceAsStream("/config.properties")) {
            properties.load(inputStream);
        } catch (Exception e) {
            logger.error("Unable to read config.properties", e);
        }
    }

    public String getBaseUrl() {
        return properties.getProperty("baseUrl");
    }

    public String getSearchPath(){
        return properties.getProperty("searchPath");
    }
}
