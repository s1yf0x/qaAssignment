package com.herokuapp.waakoopserver.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {

    private String provider;
    private String title;
    private String url;
    private String brand;
    private String unit;
    private String promoDetails;
    private String image;
    private String isPromo;
    private float price;

    public boolean isPromo() {
        return Boolean.parseBoolean(isPromo);
    }

}
