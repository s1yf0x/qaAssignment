# Serenity Testing Assignment

## Introduction
This is my variation of the task for API Testing using Serenity BDD, Cucumber & Java.

## Prerequisites
Java 1.8 + Maven

## Get started
- `git clone https://gitlab.com/s1yf0x/qaAssignment.git`
- `cd qaAssignment`
- `mvn clean verify -Dtags="@smoke"`

Or

- `mvn clean verify` for run all tests (now all tests have `@smoke` tag)
- `mvn serenity:aggregate` if tests have failures (it definitely has, please look at PS section)

## Reports
The project provides for the generation of Serenity reports.
They are automatically generated in `target/site/serenity`.
In case of failed tests, you must execute `mvn serenity:aggregate`

## CI
This repository implement CI.
For running you need to have access to this repository.
Report available as artifact after pipeline was executed.

## Shortly about how to add new tests

- Put new scenarios to `*.feature` file located somewhere in `src/test/resources/features` using gherkin language.
- Create a new Java class at `src/test/java/com/herokuapp/waakoopserver/stepDefinitions` or use existing Java implementations of steps.
- Use existing implementations of API in `SearchApi.class` located in `com/herokuapp/waakoopserver/api` or create new.
- Add some new assertions to `com/herokuapp/waakoopserver/assertions` existing class or create new one.

Or

- Just put you scenarios to `*.feature` file located somewhere in `src/test/resources/features` using gherkin language and let me know, I will implement them :relaxed:

## Shortly about structure of project
- Package `api` contain all calls to API
- Package `assertions` contain some custom assertions
- Package `models` contain models of JSON response bodies
- Package `stepDefinition` contain step definitions for cucumber scenarios
- Package `utilities` contain some utilities classes for com.herokuapp.waakoopserver.assertions, configuration and rest

## Changes
- Fixed typos
- Cleaned up `pom.xml`.
- Updated all dependencies.
- Removed all the gradle stuff.
- Removed/changed useless classes.
- Added models.
- Implemented/reimplemented step definitions.
- Renamed classes and methods.
- Added a simple configuration file and Configuration.class.
- Implemented assertions.

## P.S.
I faced a question that I would love to discuss with you.

In these tests, the validation of correctness of the search occurs exclusively by the `title` field. But it is obvious that the products that we receive with this or that request will also go there because the description of the product that is contained in the link in the `url` field also contains the word that we use in the request. But I have come across some products in the title and description of which the requested word is missing on the site, but the word is present on the page in a hidden element.

For example:
```json
   {
       "provider": "Vomar",
       "title": "Bonbébé Appel Perzik Banaan Pouch 6 Maanden 90 g",
       "url": "https://www.vomar.nl/producten/vers/fruit/any/138806",
       "brand": "Bonbébé",
       "price": 0.81,
       "unit": "90 g",
       "isPromo": false,
       "promoDetails": "",
       "image": "https://files.vomar.nl/articles/Bonb-b-Appel-Perzik-Banaan-Pouch-6-Maanden-90-g-8710624283353-1-637431071301419928.png"
   }
```

Also, links to some products are not available (error 403 for example)

For example:
```json
   {
       "provider": "Vomar",
       "title": "Pickwick Variatie Groene Thee 20 Stuks",
       "url": "https://www.vomar.nl/producten/vers/fruit/any/137148",
       "brand": "Pickwick",
       "price": 1.89,
       "unit": "20 Stuks",
       "isPromo": false,
       "promoDetails": "",
       "image": "https://files.vomar.nl/articles/Pickwick-Variatie-Groene-Thee-20-Stuks-8711000895979-1-637572096923478167.png"
   }
```