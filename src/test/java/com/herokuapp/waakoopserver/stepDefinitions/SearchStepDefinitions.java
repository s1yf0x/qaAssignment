package com.herokuapp.waakoopserver.stepDefinitions;

import com.herokuapp.waakoopserver.api.SearchApi;
import com.herokuapp.waakoopserver.assertions.ProductAssertions;
import com.herokuapp.waakoopserver.utilities.RestUtil;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;

import static com.herokuapp.waakoopserver.assertions.ErrorAssertion.*;
import static com.herokuapp.waakoopserver.assertions.ProductAssertions.*;
import static org.junit.jupiter.api.Assertions.assertAll;


public class SearchStepDefinitions {

    Response searchResponse;

    @Steps
    SearchApi searchApi;

    @Steps
    RestUtil restUtil;

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String arg0) {
        searchResponse = restUtil.get(arg0);
    }

    @Then("he sees the results displayed for apple")
    public void heSeesTheResultsDisplayedForApple() {
        assertAll(
                () -> checkStatusOk(searchResponse),
                () -> validateTitleContain(searchResponse, "apple")
        );
    }

    @Then("he sees the results displayed for mango")
    public void heSeesTheResultsDisplayedForMango() {
        assertAll(
                () -> checkStatusOk(searchResponse),
                () -> validateTitleContain(searchResponse, "mango")
        );

    }

    @Then("he does not see the results")
    public void heDoesNotSeeTheResults() {
        assertAll(
                () -> checkStatusNotFound(searchResponse),
                () -> validateIsError(searchResponse),
                () -> validateErrorMessage(searchResponse),
                () -> validateServedBy(searchResponse)
        );
    }

    @Given("User search {string}")
    public void userSearchProduct(String arg0) {
        searchResponse = searchApi.searchProduct(arg0);
    }

    @Then("User get list of products contain {string} in title")
    public void userGetListOfProductsContainProductInTitle(String arg0) {
        assertAll(
                () -> checkStatusOk(searchResponse),
                () -> validateTitleContain(searchResponse, arg0),
                () -> validateProviderNotNull(searchResponse),
                () -> validateUrlNotNull(searchResponse),
                () -> validateBrandNotNull(searchResponse),
                () -> validatePriceNotZero(searchResponse),
                () -> validateUnitNotNull(searchResponse),
                () -> validatePromoStatus(searchResponse),
                () -> validateImage(searchResponse));
    }

    @Then("User get error on request {string}")
    public void userGetError(String arg0) {
        assertAll(
                () -> checkStatusNotFound(searchResponse),
                () -> validateIsError(searchResponse),
                () -> validateErrorMessage(searchResponse),
                () -> validateRequestedItemInError(arg0, searchResponse),
                () -> validateServedBy(searchResponse)
        );
    }
}
