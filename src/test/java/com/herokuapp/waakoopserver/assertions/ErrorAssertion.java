package com.herokuapp.waakoopserver.assertions;

import com.herokuapp.waakoopserver.models.Error;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ErrorAssertion extends Assert {

    protected ErrorAssertion() {
    }

    private static Logger logger = LoggerFactory.getLogger(ProductAssertions.class);


    public static void checkStatusNotFound(Response searchResponse) {
        logger.info("Validating response code");
        assertEquals("Response code is not 404", HttpStatus.SC_NOT_FOUND, searchResponse.statusCode());
    }

    public static void validateIsError(Response searchResponse) {
        logger.info("Validating that field 'error=true' for response code 404");
        assertTrue(searchResponse.as(Error.class).getDetail().isError());
    }

    public static void validateErrorMessage(Response searchResponse) {
        logger.info("Validating that field 'message' not null for response code 404");
        assertNotNull(searchResponse.as(Error.class).getDetail().getMessage());
    }

    public static void validateServedBy(Response searchResponse) {
        logger.info("Validating that field 'servedBy' not null for response code 404");
        assertNotNull(searchResponse.as(Error.class).getDetail().getServedBy());
    }

    public static void validateRequestedItemInError(String arg0, Response searchResponse) {
        logger.info("Validating that field 'requestedItem' contain requested item for response code 404");
        assertEquals(arg0, searchResponse.as(Error.class).getDetail().getRequestedItem());

    }
}
