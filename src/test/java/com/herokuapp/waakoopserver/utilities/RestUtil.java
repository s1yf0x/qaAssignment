package com.herokuapp.waakoopserver.utilities;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class RestUtil {

    Configuration configuration = new Configuration();

    @Step
    public Response search(String product) {
        return SerenityRest
                .given()
                .get(configuration.getBaseUrl()
                        + configuration.getSearchPath()
                        + product);
    }

    @Step
    public Response get(String arg0) {
        return SerenityRest.get(arg0);
    }
}
