package com.herokuapp.waakoopserver.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Detail {

    String error;
    String message;
    @JsonProperty("requested_item")
    String requestedItem;
    @JsonProperty("served_by")
    String servedBy;

    public boolean isError() {
        return Boolean.parseBoolean(error);
    }
}
