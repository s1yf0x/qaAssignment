package com.herokuapp.waakoopserver.models;

import lombok.Getter;

@Getter
public class Error {
    private Detail detail;
}
