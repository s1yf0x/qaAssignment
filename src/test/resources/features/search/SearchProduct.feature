Feature: Search for the product

### Original scenario (wasn't modified)
  @smoke
  Scenario: Original scenario
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple"
    Then he sees the results displayed for apple
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango"
    Then he sees the results displayed for mango
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/car"
    Then he does not see the results

### Positive scenario with test data
  @smoke
  Scenario Outline: Positive search scenario
    Given User search "<product>"
    Then User get list of products contain "<product>" in title
    Examples:
      | product |
      | apple   |
      | mango   |
      | tofu    |
      | water   |

### Negative scenario with test data
  @smoke
  Scenario Outline: Negative search scenario
    Given User search "<product>"
    Then User get error on request "<product>"
    Examples:
      | product |
      | Apple   |
      | 123     |
      | %$*@(   |