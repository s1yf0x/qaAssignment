package com.herokuapp.waakoopserver.assertions;

import com.herokuapp.waakoopserver.models.Error;
import com.herokuapp.waakoopserver.models.Product;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class ProductAssertions {

    protected ProductAssertions() {
    }

    private static Logger logger = LoggerFactory.getLogger(ProductAssertions.class);

    @Step
    public static void checkStatusOk(Response searchResponse) {
        logger.info("Validating response code");
        assertEquals("Response code is not 200", HttpStatus.SC_OK, searchResponse.statusCode());
    }

    @Step
    public static void validateTitleContain(Response searchResponse, String productName) {
        logger.info("Looking for {} in title field", productName);
        Arrays.stream(searchResponse.as(Product[].class)).forEach(productModel -> {
            assertTrue(productModel.getTitle().toLowerCase().contains(productName),
                    "Product title is '" + productModel.getTitle() + "' doesn't contain requested word '" + productName + "'");
        });
    }

    @Step
    public static void validateProviderNotNull(Response searchResponse) {
        Arrays.stream(searchResponse.as(Product[].class)).forEach(product -> {
            logger.info("Validating that field 'provider' not null");
            assertNotNull(product.getProvider(), "Provider for product with title '" + product.getTitle() + "' is null");
        });
    }

    @Step
    public static void validateUrlNotNull(Response searchResponse) {
        Arrays.stream(searchResponse.as(Product[].class)).forEach(product -> {
            logger.info("Validating that field 'url' not null");
            assertNotNull(product.getUrl(), "Url is '" + product.getTitle() + "' is null");
        });
    }

    @Step
    public static void validateBrandNotNull(Response searchResponse) {
        Arrays.stream(searchResponse.as(Product[].class)).forEach(product -> {
            logger.info("Validating that field 'brand' not null");
            assertNotNull(product.getBrand(), "Brand is '" + product.getTitle() + "' is null");
        });
    }

    @Step
    public static void validatePriceNotZero(Response searchResponse) {
        Arrays.stream(searchResponse.as(Product[].class)).forEach(product -> {
            logger.info("Validating that field 'price' not zero");
            assertNotEquals(0, product.getPrice(), "Price is '" + product.getTitle() + "' is zero");
        });
    }

    @Step
    public static void validateUnitNotNull(Response searchResponse) {
        Arrays.stream(searchResponse.as(Product[].class)).forEach(product -> {
            logger.info("Validating that field 'unit' not null");
            assertNotNull(product.getUnit(), "Unit is '" + product.getTitle() + "' is null");
        });
    }

    @Step
    public static void validatePromoStatus(Response searchResponse) {
        Arrays.stream(searchResponse.as(Product[].class)).forEach(product -> {
            logger.info("Checking if product has promo");
            if (product.isPromo()) {
                logger.info("Field isPromo 'true'");
                logger.info("Validating that field 'promoDetails' not empty");
                assertNotEquals("Promo details for product '" + product.getTitle() + "' empty with 'isPromo=true'", "", product.getPromoDetails());
            } else {
                logger.info("Field isPromo is 'false'");
                logger.info("Validating that field 'promoDetails' empty");
                assertEquals("Promo details for product '" + product.getTitle() + "' not empty with isPromo=false", "", product.getPromoDetails());
            }
        });
    }

    @Step
    public static void validateImage(Response searchResponse) {
        Arrays.stream(searchResponse.as(Product[].class)).forEach(product -> {
            logger.info("Validating that field 'image' not null");
            assertNotNull(product.getImage(), "Image is '" + product.getTitle() + "' is null");
        });
    }
}
